import React from 'react';
import Modal from '../../components/UI/Modal/Modal';

const Fragment = React.Fragment;

const withErrorHandler = (WrapperComponent, axios) => {
	return props => {
		return ( 
			<Fragment>
				<Modal show>
					Something didn't work
				</Modal> 
				<WrapperComponent {...props} />
			</Fragment>

			)
	}
}

export default withErrorHandler;
