import React, {Component} from 'react';
import Order from '../../components/Order/Order';
import axios from '../../axios-orders.js';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler.js';
import Spinner from '../../components/UI/Spinner/Spinner.js';

class Orders extends Component {
  state = {
    orders: [],
    loading: true,
  };
  componentDidMount() {
    axios
      .get('/orders.json')
      .then(res => {
        const fetchedOrders = [];
        for (let key in res.data) {
          fetchedOrders.push({
            ...res.data[key],
            id: key,
          });
        }
        this.setState({loading: false, orders: fetchedOrders});
      })
      .catch(err => {
        this.setState({loading: false});
      });
  }
  render() {
    let orders = <Spinner />;
    if (!this.state.loading)
      orders = this.state.orders.map(o => <Order {...o} key={o.id} />);
    return (
      <div>
        {orders}
      </div>
    );
  }
}

export default withErrorHandler(Orders, axios);
