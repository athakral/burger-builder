import React from 'react';
import classes from './Order.css';

const order = props => {
  let ingredients = [];
  for (let key in props.ingredients)
    ingredients.push({name: key, count: props.ingredients[key]});
  const ingredientOut = ingredients.map(i => (
    <span
      style={{
        textTransform: 'capitalize',
        display: 'inline-block',
        margin: '0 8px',
        border: '1px solid #ccc',
        padding: '5px',
      }}>
      {i.name} ({i.count})
    </span>
  ));
  return (
    <div className={classes.Order}>
      <p> Ingredients: {ingredientOut}</p>
      <p>
        Price: <strong>{props.price.toFixed(2)}</strong>
      </p>
    </div>
  );
};
export default order;
