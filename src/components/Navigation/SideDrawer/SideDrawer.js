import React from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../../Navigation/NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import Backdrop from '../../UI/Backdrop/Backdrop';

const Fragment = React.Fragment;

const sideDrawer = props => (
  <Fragment>
    <Backdrop show={props.open}  clicked={props.closed}/>
    <div className={[classes.SideDrawer, props.open ? classes.Open : classes.Close].join(' ')}>
      <div className={classes.Logo}>
        <Logo />
      </div>
      <nav>
        <NavigationItems />
      </nav>
    </div>
  </Fragment>
);

export default sideDrawer;
