import React, {Component} from 'react';
import Button from '../../UI/Button/Button';

const Fragment = React.Fragment;

class OrderSummary extends Component {
	// OrderSummary can be converted into functional component
  componentWillUpdate() {
    // console.log(' Order Summary will update');
  }
  render() {
    return (
      <Fragment>
        <h3> Your Order </h3>
        <p> A delicious burger with the following ingredients: </p>
        <ul>
          {Object.keys(this.props.ingredients).map(i => (
            <li key={i}>
              <span sytle={{textTransform: 'capitalize'}}>{i}</span> :{' '}
              {this.props.ingredients[i]}
            </li>
          ))}
        </ul>
        <p>
          <strong>Total Price: {this.props.price.toFixed(2)}</strong>
        </p>
        <p> Continue to Checkout?</p>
        <Button btnType="Danger" clicked={this.props.purchaseCancelled}>
          CANCEL
        </Button>
        <Button btnType="Success" clicked={this.props.purchaseContinued}>
          CONTINUE
        </Button>
      </Fragment>
    );
  }
}
export default OrderSummary;
